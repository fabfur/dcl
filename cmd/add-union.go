package cmd

import (
	"fmt"
	"golang.org/x/exp/slices"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/jhathaway/dcl/cfg"
	"gitlab.wikimedia.org/jhathaway/dcl/kubenode"
)

func addUnionCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:               "union",
		Short:             "add node union from config",
		Args:              cobra.ExactArgs(1),
		RunE:              addUnion,
		ValidArgsFunction: addUnionComp,
	}
	return cmd
}

func addUnionComp(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
	if len(args) != 0 {
		return nil, cobra.ShellCompDirectiveNoFileComp
	}
	err := specCfgLoad()
	if err != nil {
		return nil, cobra.ShellCompDirectiveNoFileComp
	}
	var desc string
	var compReply []string
	for name, unionSet := range Cfg.NodeUnionSets {
		if unionSet.Desc == "" {
			desc = strings.Join(unionSet.Sets, "")
		} else {
			desc = unionSet.Desc
		}
		compReply = append(compReply, fmt.Sprintf("%s\t%s", name, desc))
	}
	slices.Sort(compReply)
	return compReply, cobra.ShellCompDirectiveNoFileComp
}

func addUnion(cmd *cobra.Command, args []string) error {
	var err error
	nodeUnionSetName := args[0]
	nodeSet, err := cfg.LoadUnionSet(nodeUnionSetName, Cfg)
	if err != nil {
		return err
	}
	restclient, clientset, err := kubeClientset()
	if err != nil {
		return err
	}
	return kubenode.Reconcile(nodeSet, Prune, restclient, clientset, Cfg)
}
