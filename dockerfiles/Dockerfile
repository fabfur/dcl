ARG DEBIAN_CODENAME
FROM debian:${DEBIAN_CODENAME}
ARG DEBIAN_CODENAME

# Only generate the UTF-8 locale
COPY locale.gen /etc/locale.gen

# Bullseye only apt setup, can't use COPY /root as copy modifies existing
# directory perms
COPY debian/${DEBIAN_CODENAME}/root/etc/apt/ /etc/apt/

RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update --quiet && \
    apt-get install --quiet --assume-yes --no-install-recommends \
    # systemd
    systemd \
    systemd-sysv \
    dbus \
    # bootstrap deps
    rsync \
    lsb-release \
    ca-certificates \
    git \
    curl \
    # puppet client
    puppet \
    # networking custom fact
    iproute2 \
    locales \
    ncurses-term \
    # not present in Debian's container image
    man-db \
    # dev
    vim \
    tree \
    procps \
    iputils-ping \
    libcap2-bin \
    netcat-openbsd \
    strace \
    less

# Ensure we have puppet 7
RUN bash -c '[[ "$(puppet agent --version)" =~ ^7\. ]]'

# Avoid non-root users accidentally running puppet, the puppet CA api is
# unprotected, so regular users can submit a signing request, we could add
# sometype of policy to avoid this:
# - https://www.puppet.com/docs/puppet/7/ssl_autosign.html, but that provides poo
# - https://www.declarativesystems.com/2015/04/13/policy-based-autosigning.html
# But in practice that doesn't work very well as there is no feedback on the cli
# as to why the cert was rejected.
RUN chmod 0750 /usr/bin/puppet

# Fix ping in containers
RUN bash -c 'if [[ $(getcap $(which ping)) != "" ]]; then setcap -r $(which ping); fi'

# TODO: HACK, update facter, https://github.com/puppetlabs/facter/pull/2574
COPY debian/${DEBIAN_CODENAME}/debs/facter_4.3.2-1~wmf1_all.deb /tmp/
RUN DEBIAN_FRONTEND=noninteractive \
    apt-get install --quiet --assume-yes --no-install-recommends /tmp/facter_*.deb

# Add our own pki certs
COPY pki/wmf-certificates_*.deb /tmp/
RUN DEBIAN_FRONTEND=noninteractive \
    apt-get install --quiet --assume-yes --no-install-recommends /tmp/wmf-certificates_*.deb

# Generate our locale and set as the default
RUN locale-gen
RUN update-locale LANG=en_US.UTF-8

CMD [ "/sbin/init" ]
