# Base images

## Building

Building images only works on Linux at present, last tested with Podman
v4.6.2 running on Debian sid.

*NOTE:* building images requires running as root because we need to
create a local network. This local network is used to run our
puppetserver during building.

    # Build images
    $ sudo make PUPPET_SRC=~/src/wmf/puppet LABS_PRIVATE_SRC=~/src/wmf/labs-private

## Publishing

### Minikube

    # Set the image pull policy to never
    # images:
    #   pull-policy: Never
    vi ~/.config/dcl/dcl.yaml
    # Load images into Minikube
    $ ./build -l minikube

### Registry: docker-registry.wikimedia.org

    # Load images into docker-registry.wikimedia.org via
    # build2001.codfw.wmnet
    $ ./build -l remote

### MacOS

    # Load images into quickemu qemu hackintosh
    $ ./build -l macos
