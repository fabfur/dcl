package unix

import (
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	"net/netip"
	"testing"
)

func TestParseIpRoute(t *testing.T) {
	t.Parallel()

	tests := []struct {
		input string
		spec  IpRoute
	}{
		{
			`route to: 192.168.105.9
destination: 192.168.105.9
  interface: bridge100
      flags: <UP,HOST,DONE,LLINFO,WASCLONED,IFSCOPE,IFREF>
 recvpipe  sendpipe  ssthresh  rtt,msec    rttvar  hopcount      mtu     expire
       0         0  38930017         4         5         0      1500      1186
`,
			IpRoute{
				Dst: netip.MustParseAddr("192.168.105.9"),
				Dev: "bridge100",
			},
		},
	}

	for _, test := range tests {
		actual, _ := parseDarwinIpRoute(test.input)
		if diff := cmp.Diff(test.spec, actual, cmpopts.EquateComparable(netip.Addr{}, netip.Prefix{})); diff != "" {
			t.Errorf("groupedResourcesNodeFiles() mismatch (-expected +actual):\n%s", diff)
		}
	}
}

func TestParseRoute(t *testing.T) {
	t.Parallel()

	tests := []struct {
		input string
		spec  Route
	}{
		{
			`route to: 10.192.0.0
destination: 10.192.0.0
       mask: 255.255.252.0
    gateway: 192.168.105.9
  interface: bridge100
      flags: <UP,GATEWAY,DONE,STATIC,PRCLONING>
 recvpipe  sendpipe  ssthresh  rtt,msec    rttvar  hopcount      mtu     expire
       0         0         0         0         0         0      1500         0
`,
			Route{
				Dst:     netip.MustParsePrefix("10.192.0.0/22"),
				Gateway: netip.MustParseAddr("192.168.105.9"),
				Dev:     "bridge100",
			},
		},
	}

	for _, test := range tests {
		actual, _ := parseDarwinRoute(test.input)
		if diff := cmp.Diff(test.spec, actual, cmpopts.EquateComparable(netip.Addr{}, netip.Prefix{})); diff != "" {
			t.Errorf("groupedResourcesNodeFiles() mismatch (-expected +actual):\n%s", diff)
		}
	}
}
