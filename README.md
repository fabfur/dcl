# dcl (Data Center Laboratory)

Using `dcl` you can develop Puppet code and test it locally in a
containerized facsimile of production. `dcl` mounts your puppet code
directly onto the Puppet server so the changes you make are available
immediately.

![dcl demo](demo/demo.svg)

## Usage

    # Source shell completion
    source <(dcl completion $(basename $SHELL))

    # Start lab, (creates a puppetserver, puppetdb, & pki host)
    dcl lab start

    # Add a test host
    dcl add nodes -c bookworm sretest1003.eqiad.wmnet

    # Check if your hosts are up
    dcl list

    # SSH to new host and run puppet
    ssh sretest1003 sudo puppet agent -t

    # Monitor your puppet repo, and run a puppet noop whenever you make an
    # edit with [entr](https://eradman.com/entrproject/)!
    printf '%s\n' ~/src/wmf/puppet/** |
        entr -ac ssh sretest1003 sudo puppet agent -t --noop

## Install

### Debian

Tested on Debian Bookworm

    # Install dependencies
    # **NOTE**: systemd-resolved is needed for split DNS, so linux can query
    # the kubernetes DNS server for the dcl containers.
    sudo apt-get install nfs-kernel-server kubernetes-client \
        systemd-resolved podman golang make

    # Install minikube
    curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
    sudo dpkg -i minikube_latest_amd64.deb

    # Build and install dcl
    git clone https://gitlab.wikimedia.org/jhathaway/dcl.git
    cd dcl
    make install

    # Copy sample config
    mkdir -p ${XDG_CONFIG_HOME:-~/.config/dcl}
    cp dcl.yaml ${XDG_CONFIG_HOME:-~/.config/dcl}/

    # Copy ssh config
    cp ssh/* ~/.ssh/
    chmod 600 ~/.ssh/id_rsa_dcl_cert
    # Add config to your global SSH section
    echo 'Include config_dcl' >> ~/.ssh/config

    # Clone puppet source repos
    # *NOTE*: You may also use your existing clones or change paths if desired
    mkdir -p src/wmf
    git clone "https://gerrit.wikimedia.org/r/operations/puppet" ~/src/wmf/puppet
    git clone "https://gerrit.wikimedia.org/r/labs/private/" ~/src/wmf/labs-private

    # Update sample config with altered repo paths if needed
    vi ${XDG_CONFIG_HOME:-~/.config/dcl}/dcl.yaml

    # Start lab
    dcl lab start

### macOS

Tested on macOS Ventura

    # Install dependencies
    brew install go minikube qemu socket_vmnet
    # Start socket_vmnet
    brew tap homebrew/services
    HOMEBREW=$(which brew) && sudo ${HOMEBREW} services start socket_vmnet

    # Build and install dcl
    git clone https://gitlab.wikimedia.org/jhathaway/dcl.git
    cd dcl
    make install

    # Copy sample config
    mkdir -p ~/Library/Application\ Support/dcl
    cp dcl.yaml ~/Library/Application\ Support/dcl/

    # Copy ssh config
    cp ssh/* ~/.ssh/
    chmod 600 ~/.ssh/id_rsa_dcl_cert
    # Add config to your global SSH section
    echo 'Include config_dcl' >> ~/.ssh/config

    # Clone puppet source repos
    # *NOTE*: May also use existing clones or change paths if desired
    mkdir -p src/wmf
    git clone "https://gerrit.wikimedia.org/r/operations/puppet" ~/src/wmf/puppet
    git clone "https://gerrit.wikimedia.org/r/labs/private/" ~/src/wmf/labs-private

    # Update sample config with altered repo paths if needed
    vi ~/Library/Application\ Support/dcl/dcl.yaml

    # Start lab
    dcl lab start

## Firewall & Routing

You made need to adjust your firewall and routing settings to allow
minikube to pull down images and for the containers to reach the
internet.

- Linux

      # If you drop INPUT by default in iptables you will need to allow INPUT from the podman interface
      sudo iptables -A INPUT -i podman+ -j ACCEPT
      # IP forwarding must be enabled to route packets to the container IPs
      sudo sysctl net.ipv4.ip_forward=1

## SSH

The SSH config allows passwordless SSH access with only a hostname. This
relies on SSH adding the domain name to the hostname, this may cause
conflicts with your own DNS search settings.

## Base images

Base images are currently published to,
<https://docker-registry.wikimedia.org>, And will be loaded
automatically by `dcl`. If you want debug or develop the base images
please see the `dockerfiles/README.md`.
